const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe ("Our first unit tests", () => {
    before(() => {
        // Initialization
        // Create objects... Etc..
        console.log("Initialising tests.");
    });
    
    // Tests
    it("Can add 1 and 2 together", () => {
        expect(mylib.add(1,2)).equal(3), "1 + 2 is not 3, for some reason?";
    });

    it("Dividing two values", () => {
        expect(mylib.divide(10,0));
    });

    after(() => { 
        console.log("Testing completed!")
    });
});