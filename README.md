# Running the unit tests

To run the tests, navigate to directory folder of the project and run the command in the terminal: 
'npm run test'

After that, it will check all the steps and print out the information on whether you pass or fail something.