/** Basic Arithmetic Operations. */

const mylib = {
    /** Multiline Arrow Function. */
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    
    substract: (a, b) => {
        return a - b;
    },

    divide: (dividend, divisor) => {
        if (divisor === 0) {
          throw new Error('Cannot divide by 0');
        }
        return dividend / divisor;
    },

    /** Singleline Arrow Function. */
    // divide: (dividend, divisor) => dividend / divisor,

    /** Regular Function. */
    multiply: function(a, b) {
        return a * b;
    }
};

module.exports = mylib;
